#!/bin/bash -e

. ./start.sh norun

./portail-citoyen2 validate
(pylint -f parseable --rcfile /var/lib/jenkins/pylint.django.rc portail_citoyen2/ | tee pylint.out) || /bin/true
