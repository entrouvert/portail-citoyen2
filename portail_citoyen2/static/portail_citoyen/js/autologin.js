function autologin(authentic_url, login_url, next_url) {
  var document = window.document;
  var callback_name = 'callback'+Math.floor(Math.random()*100000+1);
  window[callback_name] = function(is_logged) {
    var s;
    if (is_logged) {
      if (login_url.indexOf('?') == -1) {
        s = '?';
      } else {
        s = '&';
      }
      window.location.href = login_url + s + 'next=' + encodeURI(next_url);
    }
  }
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.async = true;
  script.src = authentic_url + '/accounts/logged-in/?callback=' + callback_name;
  document.getElementsByTagName('head')[0].appendChild(script);
}
