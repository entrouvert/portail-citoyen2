import json
from datetime import timedelta

from django.views.generic import TemplateView
from django.http import HttpResponse
from django.utils.timezone import now

from django.contrib.auth import get_user_model
from django.contrib.auth import logout as auth_logout

from allauth.account.views import LogoutView
from allauth.account.adapter import get_adapter

class PCLogoutView(LogoutView):
    def get_redirect_url(self):
        return (self.request.REQUEST.get(self.redirect_field_name) or
                get_adapter().get_logout_redirect_url(self.request))

    def logout(self):
        # override django-allauth behaviour to prevent it to put a
        # message(using django.contrib.messages)
        auth_logout(self.request)

logout = PCLogoutView.as_view()

def stats(request):
    User = get_user_model()
    last_day = now() - timedelta(days=1)
    last_hour = now() - timedelta(hours=1)
    qs = User.objects.all()
    what = {
            'users_count': qs.count(),
            'users_last_connection_less_than_one_day_ago': qs.filter(last_login__gte=last_day).count(),
            'user_last_connection_less_than_one_hour_ago': qs.filter(last_login__gte=last_hour).count(),
    }
    return HttpResponse(json.dumps(what), mimetype='application/json')


class TemplateEZTView(TemplateView):
    template_name = 'portail_citoyen/template_ezt.html'

template_ezt = TemplateEZTView.as_view()
