from django.views.decorators.cache import never_cache
from django.http import HttpResponseRedirect
from django.utils.http import urlencode
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib import admin

@never_cache
def login(request, extra_context=None):
    query = urlencode({REDIRECT_FIELD_NAME: request.build_absolute_uri()})
    url = '{0}?{1}'.format(settings.LOGIN_URL, query)
    return HttpResponseRedirect(url)

admin.site.login = login
