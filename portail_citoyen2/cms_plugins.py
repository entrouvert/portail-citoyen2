from django.utils.translation import ugettext as _
from django.core.exceptions import ImproperlyConfigured
from django.forms import ModelForm


from cms.plugin_base import CMSPluginBase

from . import utils

class FormPluginBase(CMSPluginBase):
    form_class = None
    no_cancel_button = True
    submit_text = None
    add_form_prefix = True
    cache = False

    def get_form_class(self, request, context, instance, placeholder):
        return self.form_class

    def render(self, context, instance, placeholder):
        request = context['request']
        class_name = self.__class__.__name__.lower()
        context['submit_name'] = submit = 'submit-{class_name}-{instance_id}'.format(
                class_name=class_name,
                instance_id=instance.id)
        context['submit_value'] = self.submit_text or _('Modify')
        context['instance'] = instance
        context['no_cancel_button'] = self.no_cancel_button
        kwargs = {}
        if self.add_form_prefix:
            kwargs['prefix'] = class_name
        form_class = self.get_form_class(request, context, instance,
                placeholder)
        if issubclass(form_class, ModelForm):
            if not hasattr(self, 'get_object'):
                raise ImproperlyConfigured('Your plugin class is missing a get_object method but use a ModelForm')
            kwargs['instance'] = context['object'] = self.get_object(request, context, instance, placeholder)
        if utils.callable_has_arg(form_class.__init__, 'plugin_instance'):
            kwargs['plugin_instance'] = instance
        if utils.callable_has_arg(form_class.__init__, 'request'):
            kwargs['request'] = request
        if request.method == 'POST' and submit in request.POST:
            form  = form_class(data=request.POST, **kwargs)
            if form.is_valid():
                form.save()
        else:
            form  = form_class(**kwargs)
        context['form'] = form
        return context
