from django.core.exceptions import ImproperlyConfigured

import os
import logging.handlers

from . import plugins

gettext_noop = lambda s: s
# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'portail_citoyen2.wsgi.application'
DEBUG = 'DEBUG' in os.environ
DEBUG_PROPAGATE_EXCEPTIONS = 'DEBUG_PROPAGATE_EXCEPTIONS' in os.environ
TEMPLATE_DEBUG = DEBUG

PROJECT_PATH = os.path.join(os.path.dirname(__file__))
PROJECT_NAME = 'portail-citoyen2'

ADMINS = ()
if 'ADMINS' in os.environ:
    ADMINS = filter(None, os.environ.get('ADMINS').split(':'))
    ADMINS = [ admin.split(';') for admin in ADMINS ]
    for admin in ADMINS:
        assert len(admin) == 2, 'ADMINS setting must be a colon separated list of name and emails separated by a semi-colon'
        assert '@' in admin[1], 'ADMINS setting pairs second value must be emails'

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DATABASE_ENGINE', 'django.db.backends.sqlite3'),
        'NAME': os.environ.get('DATABASE_NAME', os.path.join(PROJECT_PATH, '..', PROJECT_NAME + '.db')),
        'USER': os.environ.get('DATABASE_USER', ''),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
        'HOST': os.environ.get('DATABASE_HOST', ''),
        'PORT': os.environ.get('DATABASE_PORT', '')
    }
}

# Hey Entr'ouvert is in France !!
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'
SITE_ID = 1
USE_I18N = True
USE_TZ = True

LANGUAGES = (
    ('fr', gettext_noop('French')),
)
USE_L10N = True

# Static files

STATIC_ROOT = os.environ.get('STATIC_ROOT', '/var/lib/%s/collectstatic' % PROJECT_NAME)
STATIC_URL = os.environ.get('STATIC_URL', '/static/')
MEDIA_ROOT = os.environ.get('MEDIA_ROOT', '/var/lib/%s/media' % PROJECT_NAME)
MEDIA_URL = os.environ.get('MEDIA_URL', '/media/')

# passerelle address & apikey
PASSERELLE_URL = os.environ.get('PASSERELLE_URL', '')
PASSERELLE_APIKEY = os.environ.get('PASSERELLE_APIKEY', '')

ETC_DIR = os.path.join('/etc', 'portail-citoyen')

STATICFILES_DIRS = ['/var/lib/%s/static' % PROJECT_NAME, os.path.join(PROJECT_PATH, 'static')]
if 'STATICFILES_DIRS' in os.environ:
    STATICFILES_DIRS = os.environ['STATICFILES_DIRS'].split(':') + STATICFILES_DIRS

TEMPLATE_DIRS = ['/var/lib/%s/templates' % PROJECT_NAME, os.path.join(PROJECT_PATH, 'templates')]
if os.environ.get('TEMPLATE_DIRS'):
    TEMPLATE_DIRS = os.environ['TEMPLATE_DIRS'].split(':') + TEMPLATE_DIRS


TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
    'portail_citoyen2.context_processors.template_vars',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'entrouvert.djommon.middleware.VersionMiddleware',
)

MIDDLEWARE_CLASSES = plugins.register_plugins_middleware(MIDDLEWARE_CLASSES)

ROOT_URLCONF = 'portail_citoyen2.urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'south',
    'mptt',
    'sekizai',
    'cms',
    'menus',
    'djangocms_text_ckeditor',
    'passerelle_register_plugin',
    'feed_plugin',
    'data_source_plugin',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # ... include the providers you want to enable:
    'portail_citoyen2',
)

INSTALLED_APPS = plugins.register_plugins_apps(INSTALLED_APPS)

try:
    import cmsplugin_blurp
    INSTALLED_APPS += (
        'cmsplugin_blurp',
    )
except ImportError:
    pass

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'allauth.account.auth_backends.AuthenticationBackend',
)
# auth and allauth settings
LOGIN_REDIRECT_URL = os.environ.get('LOGIN_REDIRECT_URL', '/')
LOGOUT_URL = os.environ.get('LOGOUT_URL', '/accounts/logout/')
PORTAIL_CITOYEN_TEMPLATE_VARS = {}
if 'AUTHENTIC2_URL' in os.environ:
    AUTHENTIC2_URL = os.environ.get('AUTHENTIC2_URL', '')
    PORTAIL_CITOYEN_TEMPLATE_VARS['AUTHENTIC2_URL'] = AUTHENTIC2_URL
    INSTALLED_APPS += ('portail_citoyen2.allauth_authentic2',)
    LOGIN_URL = os.environ.get('LOGIN_URL', '/accounts/authentic2/login/?process=login')
    PORTAIL_CITOYEN_TEMPLATE_VARS['LOGIN_URL'] = LOGIN_URL
    SOCIALACCOUNT_QUERY_EMAIL = True
    SOCIALACCOUNT_PROVIDERS = {
            'authentic2': { 
                'URL': AUTHENTIC2_URL + '/idp/oauth2/',
                'SCOPE': ['read', 'write'],
                },
    }
    SOCIALACOUNT_AUTO_SIGNUP = True
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_UNIQUE_EMAIL = False

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

# sessions
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_NAME = 'portail-citoyen'
SESSION_COOKIE_PATH = os.environ.get('SESSION_COOKIE_PATH', '/')
SESSION_COOKIE_SECURE = 'SESSION_COOKIE_SECURE' in os.environ

# email settings
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')
EMAIL_PORT = int(os.environ.get('EMAIL_PORT', 25))
EMAIL_SUBJECT_PREFIX = os.environ.get('EMAIL_SUBJECT_PREFIX', '[Portail citoyen]')
EMAIL_USE_TLS = 'EMAIL_USE_TLS' in os.environ
SERVER_EMAIL = os.environ.get('SERVER_EMAIL', 'root@localhost')
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL', 'ne-pas-repondre@portail-citoyen.fr')

# web & network settings
if 'ALLOWED_HOSTS' in os.environ:
    ALLOWED_HOSTS = os.environ['ALLOWED_HOSTS'].split(':')
else:
    ALLOWED_HOSTS = ('127.0.0.1', 'localhost')
USE_X_FORWARDED_HOST = 'USE_X_FORWARDED_HOST' in os.environ

if 'INTERNAL_IPS' in os.environ:
    INTERNAL_IPS = os.environ['INTERNAL_IPS'].split(':')
else:
    INTERNAL_IPS = ('127.0.0.1')

SECRET_KEY = os.environ.get('SECRET_KEY', '0!=(1kc6kri-ui+tmj@mr+*0bvj!(p*r0duu2n=)7@!p=pvf9n')
CSRF_COOKIE_SECURE = os.environ.get('CSRF_COOKIE_SECURE') == 'yes'

# add sentry handler if environment contains SENTRY_DSN
if 'SENTRY_DSN' in os.environ:
    try:
        import raven
    except ImportError:
        raise ImproperlyConfigured('SENTRY_DSN environment variable is set but raven is not installed.')
    SENTRY_DSN = os.environ['SENTRY_DSN']

else:
    SENTRY_DSN = None

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'syslog': {
            'format': 'portail-citoyen(pid=%(process)d) %(levelname)s %(name)s: %(message)s',
        },
        'syslog_debug': {
            'format': 'portail-citoyen(pid=%(process)d) %(levelname)s %(asctime)s t_%(thread)s %(name)s: %(message)s',
        },
    },
    'handlers': {
        'syslog': {
            'level': 'DEBUG',
            'class': 'entrouvert.logging.handlers.SysLogHandler', 
            'formatter': 'syslog_debug' if DEBUG else 'syslog',
            'facility': logging.handlers.SysLogHandler.LOG_LOCAL0,
            'address': '/dev/log',
            'max_length': 999,
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': [],
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'syslog_debug',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        'requests': {
            'handlers': ['mail_admins','syslog'],
            'level': 'ERROR',
            'propagate': False,
        },
        'portail_citoyen2': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False,
        },
        'django': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False,
        },
        'django.db': {
            'handlers': ['mail_admins','syslog'],
            'level': 'INFO',
            'propagate': False,
        },
        '': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': True,
        },
    }
}
SOUTH_TESTS_MIGRATE = False

# Admin tools
ADMIN_TOOLS_INDEX_DASHBOARD = 'portail_citoyen2.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'portail_citoyen2.dashboard.CustomAppIndexDashboard'
ADMIN_TOOLS_MENU = 'portail_citoyen2.menu.CustomMenu'
ADMIN_TOOLS_THEMING_CSS = 'portail_citoyen/css/admin.css'


# cms settings
CMS_TEMPLATES = (
    ('portail_citoyen/base_two_columns.html', 'Canevas sur deux colonnes'),
    ('portail_citoyen/base_one_column.html', 'Canevas sur une colonne'),
    ('portail_citoyen/base_help.html', 'Canevas de l\'aide'),
    ('portail_citoyen/template_ezt.html', 'Template EZT'),
)
if 'CMS_TEMPLATES' in os.environ:
    CMS_TEMPLATES = map(lambda x: x.split(';'),
            os.environ('CMS_TEMPLATES').split(':'))
# necessary for plugin displaying a form to have a fresh csrftoken
CMS_PERMISSION = True
CMS_PLACEHOLDER_CACHE = False
CMS_PAGE_CACHE = False
CMS_PLUGIN_CACHE = False

# Do we use memcached ?
if os.environ.get('USE_MEMCACHED', 'no') == 'yes':
    CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
                'LOCATION': '127.0.0.1:11211',
                'KEY_PREFIX': 'portail-citoyen',
                }
            }
    SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'


if os.environ.get('USE_DEBUG_TOOLBAR', 'no') == 'yes':
    try:
        import debug_toolbar
        MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
        INSTALLED_APPS += ('debug_toolbar',)
        DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}
    except ImportError:
        print "Debug toolbar missing, not loaded"

# extract any key starting with setting
for key in os.environ:
    if key.startswith('SETTING_'):
        setting_key = key[len('SETTING_'):]
        value = os.environ[key]
        try:
             value = int(value)
        except ValueError:
             pass
        globals()[setting_key] = value

JSON_CONFIG = os.path.join(ETC_DIR, 'config.json')

if os.path.exists(JSON_CONFIG):
    import json
    globals().update(json.load(JSON_CONFIG))

PY_CONFIG = os.path.join(ETC_DIR, 'config.py')

if os.path.exists(PY_CONFIG):
    execfile(PY_CONFIG, globals())



# try to import local_settings.py (useless, in theory)
try:
    from local_settings import *
except ImportError, e:
    if 'local_settings' in e.args[0]:
        pass

if not CMS_TEMPLATES:
    raise ImproperlyConfigured('You must define CMS_TEMPLATES')

if os.environ.get('MULTITENANT_MODE', 'no') == 'yes':
    try:
        from tenant_settings import *
    except ImportError:
        pass

if SENTRY_DSN is not None:
    try:
        import raven
    except ImportError:
        raise ImproperlyConfigured('SENTRY_DSN present but raven is not installed')
    RAVEN_CONFIG = {
            'dsn': SENTRY_DSN,
            }
    INSTALLED_APPS += ('raven.contrib.django.raven_compat', )
    LOGGING['handlers']['sentry'] = {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            }
    LOGGING['loggers']['']['handlers'].append('sentry')


CKEDITOR_SETTINGS = {
   'toolbar': [
        ['Undo', 'Redo'],
        ['Format', 'Styles', 'Font', 'FontSize'],
        ['CreateDiv', 'Link', 'Unlink', 'Anchor'],
        ['cmsplugins'],
        ['Source', '-', 'Maximize', 'ShowBlocks'],
        '/',
        ['Bold', 'Italic', 'Underline', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
        ['HorizontalRule'],
        ['TextColor', 'BGColor', '-', 'PasteText', 'PasteFromWord'],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Table'],
        ],
    'stylesSet': [{
        'name': 'Portail Citoyen block',
        'element': 'div',
        'attributes': {
            'class': 'portail-citoyen-block'
        }
    }]
}
