# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    no_dry_run = True

    def forwards(self, orm):
        db.rename_column('data_source_plugin_datasource', 'hash_algo', 'auth_mech')
        for ds in orm.DataSource.objects.all():
            if ds.auth_mech.startswith('sha'):
                ds.auth_mech = 'hmac-' + ds.auth_mech
                ds.save()

    def backwards(self, orm):
        db.rename_column('data_source_plugin_datasource', 'auth_mech', 'hash_algo')
        for ds in orm.DataSource.objects.all():
            if ds.auth_mech.startswith('hmac-'):
                ds.auth_mech = ds.auth_mech[5:]
                ds.save()
            if ds.auth_mech == 'oauth2':
                ds.delete()

    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'data_source_plugin.datasource': {
            'Meta': {'ordering': "('name',)", 'object_name': 'DataSource'},
            'allow_redirects': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'auth_mech': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mime_type': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'signature_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'timeout': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '1024'}),
            'verify_certificate': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'data_source_plugin.datasourceplugin': {
            'Meta': {'object_name': 'DataSourcePlugin'},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'refresh': ('django.db.models.fields.IntegerField', [], {'default': '60'}),
            'template_source': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'})
        },
        u'data_source_plugin.plugindatasource': {
            'Meta': {'ordering': "('order', 'id')", 'object_name': 'PluginDataSource'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'plugin': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['data_source_plugin.DataSourcePlugin']"}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'plugins'", 'to': u"orm['data_source_plugin.DataSource']"})
        },
        u'data_source_plugin.rawinlinetemplateplugin': {
            'Meta': {'object_name': 'RawInlineTemplatePlugin'},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'template_source': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'})
        }
    }

    complete_apps = ['data_source_plugin']
