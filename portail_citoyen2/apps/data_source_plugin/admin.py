from django.contrib import admin
from copy import copy

from cms_plugins import DataSourcePlugin as DataSourcePluginAdmin
from models import PluginDataSource, DataSource

class PluginDataSourceInlineAdmin(admin.TabularInline):
    model = PluginDataSource

class DataSourceAdmin(admin.ModelAdmin):
    list_display = [ 'name', 'mime_type', 'url' ]
    list_filter = ['mime_type']

DataSourcePluginAdmin.inlines = copy(DataSourcePluginAdmin.inlines)
DataSourcePluginAdmin.inlines.append(PluginDataSourceInlineAdmin)
admin.site.register(DataSource, DataSourceAdmin)
