from django.template import Template
from django.core.exceptions import ValidationError
from django.template.base import TemplateSyntaxError
from django.utils.translation import ugettext as _
from django.db import models

from cms.models import CMSPlugin

__all__ = [ 'DataSource', 'PluginDataSource', 'DataSourcePlugin' ]

def validate_template(value):
    try:
        Template(value)
    except TemplateSyntaxError, e:
        raise ValidationError(*e.args)


class DataSource(models.Model):
    JSON = 'application/json'
    RSS = 'application/rss+xml'
    HTML = 'text/html'
    XML = 'text/xml'

    CHOICES = (
            (JSON, _('JSON')),
            (RSS, _('RSS')),
            (HTML, _('HTML')),
            (XML, _('XML')),
    )

    HASHES = (
            ('', 'None'),
            ('hmac-sha256', 'HMAC-SHA-256'),
            ('hmac-sha1', 'HMAC-SHA-1'),
            ('oauth2', 'OAuth2'),
    )

    name = models.CharField(verbose_name=_('Name'), max_length=32)
    mime_type = models.CharField(max_length=256, verbose_name=_('MIME Type'),
            choices=CHOICES)
    url = models.URLField(verbose_name=_('URL'), max_length=1024,
            validators=[validate_template])
    auth_mech = models.CharField(verbose_name=_('Authentication mechanism'),
            max_length=16, choices=HASHES, default='', blank=True)
    signature_key = models.CharField(verbose_name=_('Signature key'),
            max_length=128, default='', blank=True)
    verify_certificate = models.BooleanField(verbose_name=_('verify '
        'certificate'), default=True, blank=True)
    allow_redirects = models.BooleanField(verbose_name=_('allows HTTP redirections'),
            help_text=_('it can improve latencies to forbid redirection follow'),
            default=True)
    timeout = models.IntegerField(verbose_name=_('timeout'),
            default=10,
            help_text=_('time in second to wait before '
                'failing to download a datasource'))

    def clean(self):
        if self.signature_key and (not self.auth_mech or not self.auth_mech.startswith('hmac-')):
            raise ValidationError(_('You must choose a hashing algorithm if '
                    'you set a signature key'))

    def __unicode__(self):
        return _('Data source {name}, url: {url} mime-type: {mime_type}').format(
            name=self.name, mime_type=self.mime_type, url=self.url)

    class Meta:
        verbose_name = _('Data source')
        verbose_name_plural = _('Data sources')
        ordering = ('name',)


class PluginDataSource(models.Model):
    source = models.ForeignKey('DataSource', related_name='plugins')
    plugin = models.ForeignKey('DataSourcePlugin', related_name='sources')
    order = models.IntegerField(default=0)

    def __unicode__(self):
        return unicode(self.source)

    class Meta:
        ordering = ('order', 'id')

class InlineTemplatePlugin(CMSPlugin):
    template_source = models.TextField(verbose_name=_('Overloaded template'),
            blank=True, default='', validators=[validate_template])

    @property
    def render_template(self):
        return Template(self.template_source)

    class Meta:
        abstract = True


class RawInlineTemplatePlugin(InlineTemplatePlugin):
    class Meta:
        verbose_name = _('Raw inline template plugin')
        verbose_name_plural = _('Raw inline template plugins')

    def __unicode__(self):
        return _('Raw inline template plugin')


class DataSourcePlugin(InlineTemplatePlugin):
    limit = models.IntegerField(verbose_name=_('Maximum entries'), default=10)
    refresh = models.IntegerField(verbose_name=_('Refresh timeout'), default=60,
            help_text=_('Number of seconds between two web service calls'))

    def copy_relations(self, old_instance):
        for source in old_instance.sources.all():
            self.sources.create(source=source.source, order=source.order)

    def __unicode__(self):
        s = _('DataSource with %d sources') % self.sources.count()
        return s
