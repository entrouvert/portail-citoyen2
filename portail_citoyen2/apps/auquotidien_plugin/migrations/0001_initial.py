# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AuQuotidienAPI'
        db.create_table(u'auquotidien_plugin_auquotidienapi', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('service_provider', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['saml.LibertyServiceProvider'])),
            ('orig', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('hash_algo', self.gf('django.db.models.fields.CharField')(default='', max_length=16, blank=True)),
            ('signature_key', self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True)),
            ('verify_certificate', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('allow_redirects', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('timeout', self.gf('django.db.models.fields.IntegerField')(default=10)),
        ))
        db.send_create_signal(u'auquotidien_plugin', ['AuQuotidienAPI'])

        # Adding model 'AuQuotidienActiveFormsPlugin'
        db.create_table(u'auquotidien_plugin_auquotidienactiveformsplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('refresh', self.gf('django.db.models.fields.IntegerField')(default=60)),
        ))
        db.send_create_signal(u'auquotidien_plugin', ['AuQuotidienActiveFormsPlugin'])

        # Adding M2M table for field apis on 'AuQuotidienActiveFormsPlugin'
        m2m_table_name = db.shorten_name(u'auquotidien_plugin_auquotidienactiveformsplugin_apis')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('auquotidienactiveformsplugin', models.ForeignKey(orm[u'auquotidien_plugin.auquotidienactiveformsplugin'], null=False)),
            ('auquotidienapi', models.ForeignKey(orm[u'auquotidien_plugin.auquotidienapi'], null=False))
        ))
        db.create_unique(m2m_table_name, ['auquotidienactiveformsplugin_id', 'auquotidienapi_id'])

        # Adding model 'AuQuotidienCategoryPlugin'
        db.create_table(u'auquotidien_plugin_auquotidiencategoryplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('refresh', self.gf('django.db.models.fields.IntegerField')(default=60)),
        ))
        db.send_create_signal(u'auquotidien_plugin', ['AuQuotidienCategoryPlugin'])

        # Adding M2M table for field apis on 'AuQuotidienCategoryPlugin'
        m2m_table_name = db.shorten_name(u'auquotidien_plugin_auquotidiencategoryplugin_apis')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('auquotidiencategoryplugin', models.ForeignKey(orm[u'auquotidien_plugin.auquotidiencategoryplugin'], null=False)),
            ('auquotidienapi', models.ForeignKey(orm[u'auquotidien_plugin.auquotidienapi'], null=False))
        ))
        db.create_unique(m2m_table_name, ['auquotidiencategoryplugin_id', 'auquotidienapi_id'])


    def backwards(self, orm):
        # Deleting model 'AuQuotidienAPI'
        db.delete_table(u'auquotidien_plugin_auquotidienapi')

        # Deleting model 'AuQuotidienActiveFormsPlugin'
        db.delete_table(u'auquotidien_plugin_auquotidienactiveformsplugin')

        # Removing M2M table for field apis on 'AuQuotidienActiveFormsPlugin'
        db.delete_table(db.shorten_name(u'auquotidien_plugin_auquotidienactiveformsplugin_apis'))

        # Deleting model 'AuQuotidienCategoryPlugin'
        db.delete_table(u'auquotidien_plugin_auquotidiencategoryplugin')

        # Removing M2M table for field apis on 'AuQuotidienCategoryPlugin'
        db.delete_table(db.shorten_name(u'auquotidien_plugin_auquotidiencategoryplugin_apis'))


    models = {
        u'attribute_aggregator.attributeitem': {
            'Meta': {'object_name': 'AttributeItem'},
            'attribute_name': ('django.db.models.fields.CharField', [], {'default': "('OpenLDAProotDSE', 'OpenLDAProotDSE')", 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output_name_format': ('django.db.models.fields.CharField', [], {'default': "('urn:oasis:names:tc:SAML:2.0:attrname-format:uri', 'SAMLv2 URI')", 'max_length': '100'}),
            'output_namespace': ('django.db.models.fields.CharField', [], {'default': "('Default', 'Default')", 'max_length': '100'}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['attribute_aggregator.AttributeSource']", 'null': 'True', 'blank': 'True'})
        },
        u'attribute_aggregator.attributelist': {
            'Meta': {'object_name': 'AttributeList'},
            'attributes': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'attributes of the list'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['attribute_aggregator.AttributeItem']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'attribute_aggregator.attributesource': {
            'Meta': {'object_name': 'AttributeSource'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'namespace': ('django.db.models.fields.CharField', [], {'default': "('Default', 'Default')", 'max_length': '100'})
        },
        u'auquotidien_plugin.auquotidienactiveformsplugin': {
            'Meta': {'object_name': 'AuQuotidienActiveFormsPlugin', '_ormbases': ['cms.CMSPlugin']},
            'apis': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auquotidien_plugin.AuQuotidienAPI']", 'symmetrical': 'False'}),
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'refresh': ('django.db.models.fields.IntegerField', [], {'default': '60'})
        },
        u'auquotidien_plugin.auquotidienapi': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'AuQuotidienAPI'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'allow_redirects': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'hash_algo': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'orig': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'service_provider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['saml.LibertyServiceProvider']"}),
            'signature_key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'timeout': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'verify_certificate': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'auquotidien_plugin.auquotidiencategoryplugin': {
            'Meta': {'object_name': 'AuQuotidienCategoryPlugin', '_ormbases': ['cms.CMSPlugin']},
            'apis': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auquotidien_plugin.AuQuotidienAPI']", 'symmetrical': 'False'}),
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'refresh': ('django.db.models.fields.IntegerField', [], {'default': '60'})
        },
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'idp.attributepolicy': {
            'Meta': {'object_name': 'AttributePolicy'},
            'allow_attributes_selection': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ask_consent_attributes': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'attribute_filter_for_sso_from_push_sources': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filter attributes of push sources with list'", 'null': 'True', 'to': u"orm['attribute_aggregator.AttributeList']"}),
            'attribute_list_for_sso_from_pull_sources': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'attributes from pull sources'", 'null': 'True', 'to': u"orm['attribute_aggregator.AttributeList']"}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'filter_source_of_filtered_attributes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'forward_attributes_from_push_sources': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_attributes_from_push_sources': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'map_attributes_of_filtered_attributes': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'output_name_format': ('django.db.models.fields.CharField', [], {'default': "('urn:oasis:names:tc:SAML:2.0:attrname-format:uri', 'SAMLv2 URI')", 'max_length': '100'}),
            'output_namespace': ('django.db.models.fields.CharField', [], {'default': "('Default', 'Default')", 'max_length': '100'}),
            'send_error_and_no_attrs_if_missing_required_attrs': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'source_filter_for_sso_from_push_sources': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'filter attributes of push sources with sources'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['attribute_aggregator.AttributeSource']"})
        },
        u'saml.libertyprovider': {
            'Meta': {'ordering': "('name',)", 'object_name': 'LibertyProvider'},
            'ca_cert_chain': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'entity_id': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200'}),
            'entity_id_sha1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'federation_source': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metadata': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140', 'blank': 'True'}),
            'protocol_conformance': ('django.db.models.fields.IntegerField', [], {'max_length': '10'}),
            'public_key': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '140'}),
            'ssl_certificate': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'saml.libertyproviderpolicy': {
            'Meta': {'object_name': 'LibertyProviderPolicy'},
            'authn_request_signature_check_hint': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'})
        },
        u'saml.libertyserviceprovider': {
            'Meta': {'object_name': 'LibertyServiceProvider'},
            'attribute_policy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'attribute_policy'", 'null': 'True', 'to': u"orm['idp.AttributePolicy']"}),
            'enable_following_attribute_policy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'enable_following_sp_options_policy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'liberty_provider': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'service_provider'", 'unique': 'True', 'primary_key': 'True', 'to': u"orm['saml.LibertyProvider']"}),
            'policy': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['saml.LibertyProviderPolicy']", 'null': 'True'}),
            'sp_options_policy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'sp_options_policy'", 'null': 'True', 'to': u"orm['saml.SPOptionsIdPPolicy']"})
        },
        u'saml.spoptionsidppolicy': {
            'Meta': {'object_name': 'SPOptionsIdPPolicy'},
            'accept_slo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'accepted_name_id_format': ('authentic2.saml.fields.MultiSelectField', [], {'max_length': '1024', 'blank': 'True'}),
            'ask_user_consent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'authn_request_signed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'default_name_id_format': ('django.db.models.fields.CharField', [], {'default': "'none'", 'max_length': '256'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'encrypt_assertion': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'encrypt_nameid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'federation_mode': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'forward_slo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idp_initiated_sso': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'iframe_logout_timeout': ('django.db.models.fields.PositiveIntegerField', [], {'default': '300'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'needs_iframe_logout': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prefered_assertion_consumer_binding': ('django.db.models.fields.CharField', [], {'default': "'meta'", 'max_length': '4'})
        }
    }

    complete_apps = ['auquotidien_plugin']