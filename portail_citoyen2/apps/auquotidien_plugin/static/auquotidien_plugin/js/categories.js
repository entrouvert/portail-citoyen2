$(document).ready(function () {
  $('.aq-catgr-plg').each(function (i, elt) { 
    var $plugin = $(elt);
    var $titles = $('.aq-catgr-plg-title', elt);
    var $categories = $('.aq-catgr-plg-body', elt);
    var $empty = $plugin.attr('data-empty');

    if ($plugin.data('initialized')) {
      return;
    }
    $plugin.data('initialized', 1);

    $titles.hide();

    if ($categories.length > 1) {
      if ($empty) {
        $categories.hide();
      } else {
        $categories.slice(1).hide();
      }
      // Create a selector from titles
      var $p = $('<p/>');
      var $select = $('<select/>');
      if ($empty) {
        var $option = $('<option/>');
        $option.text($empty);
        $select.append($option);
      }
      for (var i = 0; i < $titles.length; i++) {
        var $body = $($categories[i]);
        var name = $body.attr('data-name');
        var $option = $('<option/>');
        $option.text(name);
        $option.data('body', $body);
        $option.val($body[0].id);
        $select.append($option);
      }
      $p.append($select);
      $('.aq-catgr-plg-selector-container', elt).append($p);

      // Show selected categories
      var on_select_change = function(event) { 
        console.log(event.target);
        var $option = $('option:selected', event.target);
        console.log($option);
        var id = '#' + $option.val();
        console.log(id);
        $categories.not(id).hide();
        $(id).show();
      }
      $select.bind('change', on_select_change);
    }
  });
});
