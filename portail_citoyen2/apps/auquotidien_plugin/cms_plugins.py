import logging
import urlparse

from django.utils.translation import ugettext_lazy as _


from cms.plugin_pool import plugin_pool


from data_source_plugin.cms_plugins import DataSourcePlugin, Data
from data_source_plugin.models import DataSource

from .models import (
        AuQuotidienActiveFormsPlugin as AuQuotidienActiveFormsPluginModel,
        AuQuotidienCategoryPlugin as AuQuotidienCategoryPluginModel)

logger = logging.getLogger(__name__)

class FakeSource(object):
    def __init__(self, url, api):
        self.url = url
        self.mime_type = DataSource.JSON
        self.api = api

    def __getattr__(self, attribute):
        return getattr(self.api, attribute)

class AuQuotidienBasePlugin(DataSourcePlugin):
    model = AuQuotidienActiveFormsPluginModel
    render_template = None
    text_enabled = True
    inlines = []
    url_template = None

    def get_apis(self, context, instance):
        user = context['request'].user
        return instance.apis.filter(service_provider__libertyfederation__user=user, active=True)

    def get_sources(self, context, instance):
        for api in self.get_apis(context, instance):
            url = api.base_url
            url_suffix = self.url_template
            orig = api.orig
            url_suffix = url_suffix.format(provider_id=api.provider_id,
                    orig=orig)
            url = urlparse.urljoin(url, url_suffix)
            source = FakeSource(url=url, api=api)
            yield Data(source, context, 0, instance.refresh)

    def render(self, context, instance, placeholder):
        ctx = super(AuQuotidienBasePlugin, self).render(context, instance,
                placeholder)
        ctx['instance'] = instance
        return ctx


class AuQuotidienActiveFormsPlugin(AuQuotidienBasePlugin):
    model = AuQuotidienActiveFormsPluginModel
    name = _('au-quotidien active forms')
    render_template = 'auquotidien_plugin/active_forms.html'
    text_enabled = True
    inlines = []
    url_template = '/myspace/json/forms?format=json&NameID={{{{federations.service_{provider_id}.links.0}}}}&orig={orig}'


plugin_pool.register_plugin(AuQuotidienActiveFormsPlugin)


class AuQuotidienCategoryPlugin(AuQuotidienBasePlugin):
    model = AuQuotidienCategoryPluginModel
    name = _('au-quotidien categories')
    render_template = 'auquotidien_plugin/categories.html'
    text_enabled = True
    inlines = []
    url_template = '/categories?format=json&NameID={{{{federations.service_{provider_id}.links.0}}}}&orig={orig}'


plugin_pool.register_plugin(AuQuotidienCategoryPlugin)
