from django.utils.translation import ugettext as _
from django.db import models


from cms.models import CMSPlugin


from data_source_plugin.models import DataSource


__all__ = [ 'AuQuotidienAPI', 'AuQuotidienActiveFormsPlugin' ]

class AuQuotidienAPI(models.Model):
    name = models.CharField(verbose_name=_('name'), max_length=128, blank=True,
            default='')
    active = models.BooleanField(verbose_name=_('active'), default=True)
    order = models.IntegerField(verbose_name=_('order'), default=0)
    service_provider = models.ForeignKey('saml.LibertyServiceProvider',
            verbose_name=_('liberty service provider for the au-quotidien '
            'service'))
    orig = models.CharField(max_length=64, verbose_name=_('origin'))
    hash_algo = models.CharField(verbose_name=_('hashing algorithm'),
            max_length=16, choices=DataSource.HASHES, default='', blank=True)
    signature_key = models.CharField(verbose_name=_('signature key'),
            max_length=128, default='', blank=True)
    verify_certificate = models.BooleanField(verbose_name=_('verify '
        'certificate'), default=True, blank=True)
    allow_redirects = models.BooleanField(verbose_name=_('allows HTTP redirections'),
            help_text=_('it can improve latencies to forbid redirection follow'),
            default=True)
    timeout = models.IntegerField(verbose_name=_('timeout'),
            default=10,
            help_text=_('time in second to wait before '
                'failing to download a datasource'))

    @property
    def base_url(self):
        '''Base URL of the Au-Quotidien service'''
        url = self.service_provider.liberty_provider.entity_id
        return url.split('/saml/')[0]

    def __unicode__(self):
        return self.name

    @property
    def provider_id(self):
        return self.service_provider.liberty_provider.pk

    class Meta:
        verbose_name = _('au-quotidien API endpoint')
        verbose_name_plural = _('au-quotidien API endpoints')
        ordering = ('order', 'name')


class AuQuotidienActiveFormsPlugin(CMSPlugin):
    refresh = models.IntegerField(verbose_name=_('refresh timeout'), default=60,
            help_text=_('Number of seconds between two web service calls'))
    apis = models.ManyToManyField(AuQuotidienAPI, verbose_name=('au-quotidien API endpoints'))

    def __unicode__(self):
        return _('refresh {0}').format(self.refresh)

    def copy_relations(self, old_instance):
        self.apis = old_instance.apis.all()

    class Meta:
        verbose_name = _('au-quotidien active forms')
        verbose_name_plural = _('au-quotidien active forms')

class AuQuotidienCategoryPlugin(CMSPlugin):
    refresh = models.IntegerField(verbose_name=_('refresh timeout'), default=60,
            help_text=_('Number of seconds between two web service calls'))
    apis = models.ManyToManyField(AuQuotidienAPI, verbose_name=('au-quotidien API endpoints'))

    def __unicode__(self):
        return _('refresh {0}').format(self.refresh)

    def copy_relations(self, old_instance):
        self.apis = old_instance.apis.all()

    class Meta:
        verbose_name = _('au-quotidien categories')
