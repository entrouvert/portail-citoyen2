from django.contrib import admin

from models import AuQuotidienAPI

class AuQuotidienAPIAdmin(admin.ModelAdmin):
    list_display = [ 'name', 'order', 'active', 'service_provider', 'orig', 'hash_algo', 'verify_certificate', 'allow_redirects', 'timeout' ]

admin.site.register(AuQuotidienAPI, AuQuotidienAPIAdmin)
