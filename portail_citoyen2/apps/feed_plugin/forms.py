from django import forms
from django.utils.translation import ugettext as _

from . import models
from . import widgets

class FeedForm(forms.Form):
    feeds = forms.ModelMultipleChoiceField(queryset=models.Feed.objects.all(),
            label=_('Your feeds'), widget=widgets.CheckboxMultipleSelect,
            required=False)
