import logging
import threading


from django.core.cache import cache

logger = logging.getLogger(__name__)

def launch_in_thread(key, function, *args, **kwargs):
    '''Launch a function in a thread, prevent launching the same function many times using a lock'''
    key = 'thread-' + key
    created = cache.add(key, 1)
    if created:
        logger.debug('launching thread for %s', key)
        def f():
            try:
                function(*args, **kwargs)
            finally:
                cache.delete(key)
        thread = threading.Thread(target=f)
        thread.start()


