import logging

from django.utils.translation import ugettext_lazy as _


from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from . import models
from . import forms


logger = logging.getLogger(__name__)


class SelectUserFeedPlugin(CMSPluginBase):
    model = models.SelectUserFeed
    name = _('select user feeds')
    render_template = 'feed_plugin/select_user_feed.html'
    text_enabled = True

    def render(self, context, instance, placeholder):
        request = context['request']
        user = request.user
        submit = 'select-user-feed-plugin-%s' % instance.id
        if request.method == 'POST' and submit in request.POST:
            form = forms.FeedForm(data=request.POST)
            if form.is_valid():
                models.FeedPreference.objects.filter(user=user).delete()
                for feed in form.cleaned_data['feeds']:
                    models.FeedPreference.objects.get_or_create(
                            user=user, feed=feed)
        else:
            initial = dict(feeds=models.FeedPreference.objects.filter(user=user)
                .values_list('feed_id', flat=True))
            form = forms.FeedForm(initial=initial)
        context.update(dict(form=form, submit=submit))
        return context

class ShowUserFeedPlugin(CMSPluginBase):
    model = models.ShowUserFeed
    name = _('show user feeds')
    render_template = 'feed_plugin/show_user_feed.html'
    text_enabled = True

    def get_feeds(self, instance, user):
        entries = []
        feeds = models.Feed.objects.filter(feedpreference__user=user) \
                .order_by('id')
        for feed in feeds:
            feed_entries = feed.get_feed_entries(instance.limit,
                    instance.timeout)
            if feed_entries:
                for date, title, link in feed_entries:
                    entries.append((date, title, link, feed))
        entries.sort(reverse=True)
        entries = entries[:instance.limit]
        entries = [{
                'title': title,
                'link': link,
                'feed_name': feed.name,
                'color_hex': feed.color_hex,
                'css_classes': feed.css_classes
            } for date, title, link, feed in entries]
        return entries

    def render(self, context, instance, placeholder):
        request = context['request']
        entries = self.get_feeds(instance, request.user)
        context.update({'entries': entries})
        return context


plugin_pool.register_plugin(SelectUserFeedPlugin)
plugin_pool.register_plugin(ShowUserFeedPlugin)
