import hashlib
import datetime
import logging

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.html import strip_tags
from django.core.cache import cache
from django.contrib.auth import get_user_model

import feedparser

from cms.models import CMSPlugin

from . import utils

logger = logging.getLogger(__name__)


class FeedPreferenceManager(models.Manager):
    def get_by_natural_key(self, user_nk, feed_nk):
        User = get_user_model()
        return self.get(user=User.objects.get_by_natural_key(*user_nk),
                feed=Feed.objects.get_by_natural_key(*feed_nk))


class FeedPreference(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    feed = models.ForeignKey('Feed')

    objects = FeedPreferenceManager()

    def natural_key(self):
        return (self.user.natural_key(), self.feed.natural_key())

    class Meta:
        verbose_name = _('user feed subscription')
        verbose_name_plural = _('user feed subscriptions')

class SelectUserFeed(CMSPlugin):
    pass

class ShowUserFeed(CMSPlugin):
    limit = models.PositiveIntegerField(default=10)
    timeout = models.PositiveIntegerField(default=60)

class FeedManager(models.Manager):
    def get_by_natural_key(self, name, url):
        return self.get(name=name, url=url)

class Feed(models.Model):
    name = models.CharField(max_length=32, verbose_name=_('name'))
    url = models.URLField()
    color_hex = models.CharField(max_length=6,
            verbose_name=_('Color'),
            help_text=_('as an hexadecimal number'),
            blank=True)
    css_classes = models.CharField(max_length=128,
            verbose_name=_('CSS classes'),
            blank=True)

    objects = FeedManager()

    def load(self, now=None, limit=9999):
        if now is None:
            now = datetime.datetime.utcnow()
        feed = feedparser.parse(self.url)
        key = self.cache_key()
        entries = []
        for entry in feed.entries:
            for attribute in ('published_parsed', 'updated_parsed',
                    'created_parsed', 'expired_parsed'):
                date = getattr(entry, attribute, None)
                if date is not None:
                    break
            if date is None:
                continue
            title = strip_tags(entry.title.strip())
            if not title:
                title = strip_tags(entry.description.strip())
                title = ' '.join(title.split(' ')[:30])
            if not title:
                continue
            entries.append((date, title, entry.link))
        entries.sort(reverse=True)
        entries = entries[:limit]
        cache.set(key, (entries, now), 24*3600)
        duration = datetime.datetime.utcnow()-now
        logger.debug('loaded RSS feed %r in %s seconds', self.url, duration.seconds)
        return entries

    def cache_key(self):
        return hashlib.md5(self.url).hexdigest()

    def get_feed_entries(self, limit=10, timeout=3600):
        key = self.cache_key()
        now = datetime.datetime.utcnow()
        entries, stamp = cache.get(key, (None, None))
        if entries is None or now-stamp > datetime.timedelta(seconds=timeout):
            utils.launch_in_thread(key, self.load, now, limit)
        return entries

    def natural_key(self):
        return (self.name, self.url)

    class Meta:
        verbose_name = _('feed')
        verbose_name_plural = _('feeds')

    def __unicode__(self):
        return self.name
