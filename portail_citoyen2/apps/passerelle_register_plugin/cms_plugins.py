from django.utils.translation import ugettext_lazy as _


from cms.plugin_pool import plugin_pool


from portail_citoyen2.cms_plugins import FormPluginBase
from . import models, forms


class PasserelleRegisterPlugin(FormPluginBase):
    model = models.PasserelleRegisterPlugin
    name = _('passerelle register plugin')
    render_template = 'passerelle_register_plugin/plugin.html'
    text_enabled = True
    no_cancel_button = False
    form_class = forms.PasserelleRegisterForm


plugin_pool.register_plugin(PasserelleRegisterPlugin)
