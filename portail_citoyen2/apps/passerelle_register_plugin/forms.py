import logging


from django import forms


import widgets

logger = logging.getLogger(__name__)

class PasserelleRegisterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.user = self.request.user
        instance = self.instance = kwargs.pop('plugin_instance')
        try:
            subscriptions = self.subscriptions = self.instance.get_subscriptions(user=self.user.email)
        except:
            subscriptions = self.subscriptions = []
            logger.warning('unable to retrieve subscriptions for %r: %r',
                    self.user, self.user.email, exc_info=True)
        super(PasserelleRegisterForm, self).__init__(*args, **kwargs)
        self.all_choices = set()
        for subscription in subscriptions:
            if not instance.check_ressource(subscription['name']):
                continue
            choices = []
            initial = []
            for transport in subscription['transports']['available']:
                if not instance.check_transport(transport):
                    continue
                self.all_choices.add(transport)
                choices.append((transport, transport))
                if transport in subscription['transports']['defined']:
                    initial.append(transport)
            self.fields[instance.simplify(subscription['name'])] = \
                    forms.MultipleChoiceField(label=subscription['description'],
                            choices=choices, initial=initial,
                            widget=widgets.CheckboxMultipleSelect,
                            required=False)
    __init__.need_request = True
    __init__.need_plugin_instance = True


    def save(self):
        cleaned_data = self.cleaned_data
        subscriptions = []
        for key, value in cleaned_data.iteritems():
            subscriptions.append({
                'name': key,
                'transports': value
            })
        try:
            self.instance.set_subscriptions(subscriptions, user=self.user.email)
        except:
            subscriptions = self.subscriptions = []
            logger.warning('unable to save subscriptions for %r: %r',
                    self.user, self.user.email, exc_info=True)
