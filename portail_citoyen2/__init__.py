import os.path
import sys

__version__ = '1.1.1'

apps_dir = os.path.join(os.path.dirname(__file__), 'apps')
if apps_dir not in sys.path:
    sys.path.append(apps_dir)
