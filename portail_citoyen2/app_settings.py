import sys

from django.core.exceptions import ImproperlyConfigured

class AppSettings(object):
    __defaults = {
            'PORTAIL_ADMIN_URL': None,
    }

    def __init__(self, prefix):
        self.prefix = prefix

    def __getattr__(self, name):
        if name in self.__defaults:
            return self._settings(name, self.__defaults[name])
        else:
            return self._settings(name)

    @property
    def TEMPLATE_VARS(self):
        v = self._settings('TEMPLATE_VARS', {})
        try:
            from django.conf import settings
            v['LOGOUT_URL'] = settings.LOGOUT_URL
        except AttributeError:
            raise ImproperlyConfigured('LOGOUT_URL is mandatory')
        return v

    def _settings(self, name, default=Ellipsis):
        from django.conf import settings
        if default is Ellipsis:
            return getattr(settings, self.prefix + name)
        else:
            return getattr(settings, self.prefix + name, default)

app_settings = AppSettings('PORTAIL_CITOYEN_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
