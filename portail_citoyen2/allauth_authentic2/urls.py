from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns
from .provider import Authentic2Provider

urlpatterns = default_urlpatterns(Authentic2Provider)

