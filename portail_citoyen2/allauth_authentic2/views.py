import urlparse
import requests
import logging

from django.core.exceptions import ImproperlyConfigured

from allauth.socialaccount.providers.oauth2.views import (OAuth2Adapter,
                                                          OAuth2LoginView,
                                                          OAuth2CallbackView)
from .provider import Authentic2Provider
from . import app_settings

log = logging.getLogger(__name__)

class Authentic2OAuth2Adapter(OAuth2Adapter):
    provider_id = Authentic2Provider.id

    def get_url(self):
        provider = self.get_provider()
        try:
            return provider.get_settings()['URL']
        except IndexError:
            raise ImproperlyConfigured('The authentic2 provider needs an URL defined in settings')

    @property
    def access_token_url(self):
        return urlparse.urljoin(self.get_url(), 'access_token')

    @property
    def authorize_url(self):
        return urlparse.urljoin(self.get_url(), 'authorize')

    @property
    def profile_url(self):
        return urlparse.urljoin(self.get_url(), 'user-info')

    def complete_login(self, request, app, token, **kwargs):
        resp = requests.get(self.profile_url,
                headers={'authorization': 'Bearer %s' % token.token},
                verify=bool(app_settings.VERIFY))
        extra_data = resp.json()
        log.debug('received profile data: %r', extra_data)
        return self.get_provider().sociallogin_from_response(request,
                extra_data)


oauth2_login = OAuth2LoginView.adapter_view(Authentic2OAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(Authentic2OAuth2Adapter)
