import sys

class AppSettings(object):
    __defaults = {
            'ADMIN_ROLE': None,
            'VERIFY': True,
    }

    def __init__(self, prefix):
        self.prefix = prefix

    def __getattr__(self, name):
        if name in self.__defaults:
            return self._settings(name, self.__defaults[name])
        else:
            return self._settings(name)

    def _settings(self, name, default=Ellipsis):
        from django.conf import settings
        if default is Ellipsis:
            return getattr(settings, self.prefix + name)
        else:
            return getattr(settings, self.prefix + name, default)

app_settings = AppSettings('ALLAUTH_A2_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings

