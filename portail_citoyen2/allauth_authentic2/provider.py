import logging

from allauth.socialaccount import providers
from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider
from allauth.account.models import EmailAddress

from . import app_settings


log = logging.getLogger(__name__)


class Authentic2Account(ProviderAccount):
    def to_str(self):
        return self.account.uid


class Authentic2Provider(OAuth2Provider):
    id = 'authentic2'
    name = 'Authentic2'
    package = 'portail_citoyen2.allauth_authentic2'
    account_class = Authentic2Account

    def extract_uid(self, data):
        return str(data['username'])

    def extract_common_fields(self, data):
        return dict(email=data.get('email'),
                    username=data.get('username'),
                    first_name=data.get('first_name'),
                    last_name=data.get('last_name'))

    def extract_email_addresses(self, data):
        ret = [EmailAddress(email=data['email'],
                            verified=True,
                            primary=True)]
        return ret


providers.registry.register(Authentic2Provider)

from allauth.socialaccount.signals import pre_social_login
from django.dispatch import receiver

@receiver(pre_social_login)
def pre_social_login_populate_user(sender, request, sociallogin, **kwargs):
    account = sociallogin.account
    data = account.extra_data
    user = account.user
    if 'first_name' in data:
        user.first_name = data['first_name']
    if 'last_name' in data:
        user.last_name = data['last_name']
    if app_settings.ADMIN_ROLE in data.get('role', []):
        user.is_superuser = True
        user.is_staff = True
    if user.id:
        user.save()
