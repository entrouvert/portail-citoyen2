from . import app_settings

def template_vars(request):
    ctx = app_settings.TEMPLATE_VARS.copy()
    if app_settings.PORTAIL_ADMIN_URL:
        ctx['PORTAIL_ADMIN_URL'] = app_settings.PORTAIL_ADMIN_URL
    return ctx
