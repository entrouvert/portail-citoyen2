from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

from . import views, plugins

admin.autodiscover()


urlpatterns = patterns('portail_citoyen2.views',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^accounts/logout/$', 'logout', name='account_logout'),
    (r'^accounts/', include('allauth.urls')),
    url(r'^stats/$', 'stats'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )

if 'cmsplugin_blurp' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
            url('^plugin/', include('cmsplugin_blurp.urls')),
    )

urlpatterns += patterns('',
    url(r'^__template.ezt$', views.template_ezt),
    url(r'^', include('cms.urls')),
)

urlpatterns = plugins.register_plugins_urls(urlpatterns)
