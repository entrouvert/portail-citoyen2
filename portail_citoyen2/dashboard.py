from django.utils.translation import ugettext_lazy as _
from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for Compte Orleans
    """
    def init_with_context(self, context):
        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            _('Contents'),
            models=(
                'cms.models.pagemodel.Page',
                'data_source_plugin.models.DataSource',
                'feed_plugin.models.Feed',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Announces'),
            models=(
                'portail_citoyen_announces.*',
            ),
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Support'),
            children=[
                {
                    'title': _('Project site'),
                    'url': 'https://dev.entrouvert.org/projects/portail-citoyen/',
                    'external': True,
                },
                {
                    'title': _('Report a bug'),
                    'url': 'https://dev.entrouvert.org/projects/portail-citoyen/issues/new',
                    'external': True,
                },
            ]
        ))


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for Compte Orleans
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
