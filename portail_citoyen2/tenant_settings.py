import os

TENANT_MODEL = 'multitenant.Tenant'

MIDDLEWARE_CLASSES = (
    'tenant_schemas.middleware.TenantMiddleware',
    'entrouvert.djommon.multitenant.middleware.EOTenantMiddleware',
    'entrouvert.djommon.middleware.VersionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
)


TEMPLATE_LOADERS = (
    'entrouvert.djommon.multitenant.template_loader.FilesystemLoader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MULTITENANT_TEMPLATE_DIRS = (os.environ.get('MULTITENANT_TEMPLATE_DIR', '/var/lib/my_project/templates/'),)

TENANT_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'south',
    'mptt',
    'sekizai',
    'cms',
    'menus',
    'djangocms_text_ckeditor',
    'passerelle_register_plugin',
    'feed_plugin',
    'data_source_plugin',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'portail_citoyen2.allauth_authentic2',
    'portail_citoyen2',
)

SHARED_APPS = (
    'tenant_schemas',
    'entrouvert.djommon.multitenant',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
)

INSTALLED_APPS = TENANT_APPS + ('tenant_schemas', 'entrouvert.djommon.multitenant')

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': os.environ.get('DATABASE_NAME', 'portail-citoyen'),
    },
}

SOUTH_DATABASE_ADAPTERS = {
    'default': 'south.db.postgresql_psycopg2',
}
