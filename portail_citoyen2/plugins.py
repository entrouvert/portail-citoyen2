from pkg_resources import iter_entry_points
import logging

from django.conf.urls import patterns, include, url

logger = logging.getLogger(__name__)

PLUGIN_GROUP_NAME = 'portail_citoyen2.plugin'

class PluginError(Exception):
    pass

def get_plugins(*args, **kwargs):
    plugins = []
    for entrypoint in iter_entry_points(PLUGIN_GROUP_NAME):
        try:
            plugin = entrypoint.load()
        except Exception, e:
            logger.exception('failed to load entrypoint %s', entrypoint)
            raise PluginError('failed to load entrypoint %s' % entrypoint, e)
        plugins.append(plugin(*args, **kwargs))
    return plugins

def register_plugins_urls(urlpatterns):
    pre_urls = []
    post_urls = []
    for plugin in get_plugins():
        if hasattr(plugin, 'get_before_urls'):
            pre_urls.append(url('^', include(plugin.get_before_urls())))
        if hasattr(plugin, 'get_after_urls'):
            post_urls.append(url('^', include(plugin.get_after_urls())))
    pre_patterns = patterns('', *pre_urls)
    post_patterns = patterns('', *post_urls)
    return pre_patterns + urlpatterns + post_patterns

def register_plugins_apps(installed_apps):
    for plugin in get_plugins():
        if hasattr(plugin, 'get_apps'):
            apps = plugin.get_apps()
            for app in apps:
                if app not in installed_apps:
                    installed_apps += (app, )
    return installed_apps

def register_plugins_middleware(middlewares):
    for plugin in get_plugins():
        if hasattr(plugin, 'get_before_middleware'):
            pre_middleware = plugin.get_before_middleware()
            middlewares = tuple(set(pre_middleware + middlewares))
        if hasattr(plugin, 'get_after_middleware'):
            post_middleware = plugin.get_after_middleware()
            middlewares = tuple(set(post_middleware + middlewares))
    return middlewares

def init():
    for plugin in get_plugins():
        if hasattr(plugin, 'init'):
            plugin.init()

