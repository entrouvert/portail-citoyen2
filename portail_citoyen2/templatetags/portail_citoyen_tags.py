from django import template
from django.template.defaultfilters import stringfilter

from datetime import datetime, date
from decimal import Decimal

register = template.Library()

@register.assignment_tag(takes_context=True)
def user_in_group(context, group):
    user = context['user']
    return user.groups.filter(name=group).exists()


@register.assignment_tag(takes_context=True)
def user_in_group_prefix(context, group):
    user = context['user']
    return user.groups.filter(name__startswith=group).exists()


@register.filter
@stringfilter
def to_date(value, date_format='%Y-%m-%d'):
    """
    converts date string into datetime object in order to be
    used(for ex: reformated) in template
    """
    return datetime.strptime(value, date_format)


@register.filter
def to_decimal(value):
    """
    converts into Decimal object
    """
    return Decimal(value)


@register.filter
def is_past(value):
    """
    checks if date is in the past
    """
    return value.date() < date.today()
