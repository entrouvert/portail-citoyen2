#!/bin/sh

portail-citoyen-manage dumpdata --indent=2 sites cms cms.cmsplugin cmsplugin_text_wrapper \
    file googlemap link picture snippet teaser video login_plugin data_source_plugin \
    a2_service_list_plugin feed_plugin idp attribute_aggregator > demo.json
