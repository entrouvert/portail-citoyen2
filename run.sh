#!/bin/sh
BASE=`dirname $0`
PROJECT=portail-citoyen2
CTL=$BASE/${PROJECT}
VENV=$BASE/${PROJECT}-venv

if [ ! -n "$VIRTUAL_ENV" ]; then
	if [ ! -d $VENV ]; then
	$BASE/start.sh norun
	fi
	. $VENV/bin/activate
fi
export DEBUG=1
$CTL "${@:-runserver}"

