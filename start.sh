PROJECT=portail-citoyen2
CTL=${PROJECT}
VENV=${PROJECT}-venv

if [ -f /etc/debian_release ]; then
	if ! dpkg -l build-essential python-dev >/dev/null 2>&1; then
		aptitude install python-dev build-essential libffi-dev
	fi
fi

if [ "$VIRTUAL_ENV" = "" ]; then
	if which mkvirtualenv >/dev/null 2>&1; then
		workon $PROJECT || (mkvirtualenv $PROJECT; workon $PROJECT)
	else 
		if [ ! -d $VENV ]; then
			virtualenv --system-site-packages $VENV 2>/dev/null || virtualenv $VENV
		fi
		. ./$VENV/bin/activate
	fi
fi
pip install --upgrade setuptools
pip install --upgrade pip
pip install --upgrade pyOpenSSL ndg-httpsclient requests pyasn1
sed -i 's/^MAX = 64/MAX = 200/' $VIRTUAL_ENV/lib/python*/site-packages/ndg/httpsclient/subj_alt_name.py
pip install --upgrade --process-dependency-links -r requirements.txt
if [ ! -f $PROJECT.db ]; then
	./$CTL syncdb --migrate --noinput --no-initial-data
	if [ -f load-base-data.sh ]; then
		./load-base-data.sh
	fi
	if [ "$1" != "norun" ]; then
		./$CTL createsuperuser
	fi
fi
if [ "$1" != "norun" ]; then
	export DEBUG=1
	./$CTL runserver
fi
