<page xmlns="http://projectmallard.org/1.0/"
      type="guide" id="api-services" xml:lang="fr">

<info>
  <link type="guide" xref="index#api" />
  <revision docversion="0.1" date="2013-04-05" status="draft"/>
  <revision docversion="0.3" date="2013-05-01" status="draft"/>
  <credit type="author">
    <name>Frédéric Péters</name>
    <email>fpeters@entrouvert.com</email>
  </credit>
  <credit type="author">
    <name>Benjamin Dauvergne</name>
    <email>bdauvergne@entrouvert.com</email>
  </credit>
  <credit type="author">
    <name>Pierre Cros</name>
    <email>pcros@entrouvert.com</email>
  </credit>
  <desc>Remontée d'informations de services vers le portail citoyen</desc>
</info>

<title>Remontée d'informations vers le portail citoyen</title>

<section id="intro">
<title>Introduction</title>

<p>Le portail citoyen est une plate-forme dans laquelle sont agrégées des
informations provenant de sources web diverses.</p>

<p>Afin de permettre cette remontée d'information pour les services ne
disposant pas nativement d'une API exposée sur le Web, nous proposons
d'utiliser celle qui est décrite dans ce document.</p>

</section> <!-- #intro -->


<section id="requete">
<title>Requête</title>

<p>Le portail effectue des requêtes HTTP GET vers les différents services. Les
échanges atendus sont conformes au style d'architecture REST.</p>


<section id="req-ident">
<title>Requête identifiante</title>

<p>L'URI utilisée pour ces requêtes peut contenir des informations identifiant
l'usager concerné.</p>

<section id="saml">
<title>Identifiant SAML</title>

<p>Lorsque le service intègre le protocole de fédération d'identité SAML, la
requête identifiante utilisera un identifiant SAML, le NameID :</p>

<code>
https://www.example.com/eservices/pending?NameID=<var>nameid</var>
</code>
</section> <!-- #saml -->

<section id="adel">
<title>Adresse électronique</title>

<p>Lorsque le service n'intègre pas le protocole de fédération d'identité SAML,
la requête identifiante utilisera l'adresse électronique de l'utilisateur :</p>

<code>
https://www.example.com/eservices/pending?email=<var>email</var>
</code>

<note>
  <title>Adresses électroniques identiques</title>
  <p>L'utilisation de cette méthode pour identifier l'utilisateur ne fonctionne
  que si ce dernier a employé la même adresse électronique pour la création du
  compte citoyen et la création du compte sur le service.</p>
</note>
</section> <!-- #adel -->
</section> <!-- #req-ident -->

<section id="autres">
<title>Autres requêtes</title>

<p>Il est possible pour le service d'exposer davantage de données via des
URL.</p>

<section id="donnees">
<title>Données génériques (qui ne concernent pas un utilisateur particulier)</title>

<p>L'applications peut exposer des données génériques (horaires, informations,
actualités). Elle doit le faire via une URL du type :</p>

<code>https://www.example.com/eservices/horaires/</code>
</section> <!-- #donnees -->

<section id="filtres">
<title>Filtres</title>

<p>Il peut être utile pour le portail citoyen de filtrer les données récupérées
en utilisant des critères divers (catégories, localisations...) via les URL.
Les filtres doivent être compris dans les URL sous la forme (pour exposer les
horaires de Montpellier par exemple) :</p>

<code>https://www.example.com/eservices/montpellier/horaires/</code>

</section> <!-- #filtres -->
</section> <!-- #autres -->
</section> <!-- #requete -->

<section id="structure">
<title>Structure de l'information fournie par les services</title>

<p>En réponse à la requête du portail citoyen, le service doit fournir une
information structurée. Cette réponse peut prendre plusieurs formes.</p>

<section id="fils">
<title>Fils d'info</title>

<p>Le portail peut intégrer, sans que cela ne nécessite la moindre
modification, les fils d'information au format <link
href="http://en.wikipedia.org/wiki/RSS">RSS</link> et <link
href="http://en.wikipedia.org/wiki/Atom_(standard)">Atom</link> qui sont
offerts par les services.</p>

</section> <!-- #fils -->

<section id="html">
<title>HTML</title>

<p>Comme pour les fils d'infos, des blocs de code HTML fournis par les services
peuvent être intégrés directement dans le portail. À noter toutefois que ce
HTML doit être "brut" parce qu'il est filtré. Il ne peut pas contenir de
Javascript par exemple.</p>

</section> <!-- #html -->

<section id="json">
<title>JSON</title>

<p>La réponse du service à la requête du portail peut aussi être faite en
JSON.</p>

<section id="liste">
<title>Liste</title>

<p>Les données exposées au format JSON doivent être organisées sous forme d'un
tableau associatif. Les clés (variables) de ce tableau sont les suivantes :</p>

<list>
  <item><p><code>title</code> : titre (obligatoire)</p></item>
  <item><p><code>url</code> : URL (obligatoire)</p></item>
  <item><p><code>description</code> : description (facultatif)</p></item>
</list>

<p>La liste des données doit être attachées à une clé nommée <code>data</code>.
</p>

<p>Exemple :</p>

<code mime="application/json">
{ "data": [
   {"title": "Demande de bac pour ordures",
    "url": "https://eservices.example.com/dechets/demande-bac"},
   {"title": "Demande d'acte de naissance",
    "url": "https://eservices.example.com/actes/naissance",
    "Description": "Faites vos démarches sans vous déplacer"}
  ]
}
</code>

</section> <!-- #liste -->

<section id="tableau">
<title>Tableau</title>

<p>Le portail citoyen peut aussi recevoir des données qu'il affichera sous
forme de tableau.</p>

<p>La liste des données doit être attachées à une clé nommée <code>data</code>
et une clé nommée <code>columns</code> peut exister pour fournir les libellés
des colonnes.
</p>

<p>Exemple : </p>

<code mime="application/json">
{"data": [
   {"day": "Lundi", "open": "8h", "close": "17h"},
   {"day": "Mardi", "open": "8h", "close": "19h"},
   {"day": "Mercredi", "open": "9h", "close": "17h"}
  ],
 "columns": {
    "day": "Jour",
    "open": "Horaire d'ouverture",
    "close": "Horaire de fermeture"}
}
</code>

<p>Le résultat sur le portail citoyen sera l'affichage du tableau suivant :</p>

<table frame="none" rules="none" shade="rows">
<thead>
 <tr>
  <td><p>Jour</p></td>
  <td><p>Horaire d'ouverture</p></td>
  <td><p>Horaire de fermeture</p></td>
 </tr>
</thead>
<tbody>
<tr>
<td><p>Lundi</p></td><td><p>8h</p></td><td><p>17h</p></td>
</tr>
<tr>
<td><p>Mardi</p></td><td><p>8h</p></td><td><p>19h</p></td>
</tr>
<tr>
<td><p>Mercredi</p></td><td><p>9h</p></td><td><p>17h</p></td>
</tr>
</tbody>
</table>

</section> <!-- #tableau -->
	
</section> <!-- #json -->

</section> <!-- #structure -->

<section>
  <title>Signature de l'URL</title>

<p>
L'URL doit être signée via une clé partagée à configurer des deux cotés de la
liaison, la signature est du type HMAC; l'algorithme de hash à employer est
passé en paramètre.
</p>

<note><p>Si le service doit être utilisé par différents requêteurs il est
recommandé de ne pas utiliser une clé unique; il est ainsi suggéré que l'URL
attende également un paramètre précisant l'appelant, à travers, par exemple,
un paramètre supplémentaire <code>apikey</code>.
</p></note>

<note><p>En ce qui concerne l'algorithme de hash, il est préconisé d'utiliser
SHA-256 par respect du <link
href="http://references.modernisation.gouv.fr/rgs-securite">Référentiel Général
de Sécurité (RGS)</link>.</p></note>


<p>
La signature est à calculer sur la query string encodée complète, en
enlevant les paramètres terminaux <code>algo</code>, <code>timestamp</code>,
<code>nonce</code> et <code>signature</code>. La formule de calcul de la
signature est la suivante :
</p>

<code>
BASE64(HMAC-HASH(query_string+'algo=HASH&amp;timestamp=' + timestamp + '&amp;nonce=" + nonce, clé))
</code>

<list>

<item><p><code>timestamp</code> est la date dans la zone GMT au format ISO8601
en se limitant à la précision des secondes (ex : 2012-04-04T12:34:00Z),
</p></item>

<item><p>nonce est une chaîne aléatoire contenant au moins 128 bits d'entropie
(16 octets aléatoires),</p></item>

<item><p>algo est une chaîne représentant l'algorithme de hachage utilisé, sont
définis : sha1, sha256, sha512 pour les trois algorithmes correspondant.
L'utilisation d'une valeur différente n'est pas définie.</p></item>

</list>

<p>
La query string définitive est ainsi :
</p>

<code>
<var>qs_initial</var>&amp;algo=<var>algo</var>&amp;timestamp=<var>ts</var>&amp;nonce=<var>nonce</var>&amp;signature=<var>signature</var>
</code>

<p>
Pour la validation il faut contrôler :
</p>

<list>

<item><p>que la signature regénérée est identique,</p></item>

<item><p>que le timestamp est dans une fenêtre étroite par rapport à la date
courante, il est recommandé d'accepter un écart de trente secondes
maximum.</p></item>

<item><p>que le nonce n'a encore jamais été vu (associé au timestamp il suffit
de conserver les nonces pour une durée de 3 ou 4 fois la fenêtre de temps, par
exemple cinq minutes).</p></item>

</list>

<note><p>
Lors de l'utilisation de ces signatures il est important d'utiliser HTTPS pour
éviter les interceptions et de stocker les nonces pour éviter les rejeux et à
maintenir une bonne synchronisation des horloges, en utilisant par exemple le
protocole NTP.
</p></note>

</section>

</page>
